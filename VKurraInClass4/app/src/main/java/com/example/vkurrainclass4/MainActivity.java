package com.example.vkurrainclass4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TextView txtSelection;
    TextView txtEmoji;
    CheckBox chkJava;
    CheckBox chkKotlin;
    RadioGroup radMood;
    Button showSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radMood = (RadioGroup)findViewById(R.id.radioGroup4);
        txtEmoji = (TextView)findViewById(R.id.textViewEmoji);
        txtEmoji.setText("\uD83D\uDE0A");
        radMood.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton4){
                    txtEmoji.setText("\uD83D\uDE0A");
                }
                else if(checkedId == R.id.radioButton5){
                    txtEmoji.setText("\uD83D\uDE11");
                }
                else if(checkedId == R.id.radioButton6){
                    txtEmoji.setText("\uD83E\uDD7A");
                }
                else{
                    txtEmoji.setText("");
                }
            }
        });

        chkJava = (CheckBox)findViewById(R.id.checkBox3);
        chkKotlin = (CheckBox)findViewById(R.id.checkBox4);
        txtSelection = (TextView)findViewById(R.id.textViewSelection);

        CheckBox.OnCheckedChangeListener onCheckListener = new CheckBox.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String outputStr = "";
                if(chkJava.isChecked() && chkKotlin.isChecked()){
                    outputStr = "I Selected Android - Java and Android - Kotlin";
                }
                else if(chkJava.isChecked() && !chkKotlin.isChecked()){
                    outputStr = "I Selected Android - Java";
                }
                else if(!chkJava.isChecked() && chkKotlin.isChecked()){
                    outputStr = "I Selected Android - Kotlin";
                }
                else{
                    outputStr = "";
                }
                txtSelection.setText(outputStr);
            }
        };

        chkJava.setOnCheckedChangeListener(onCheckListener);
        chkKotlin.setOnCheckedChangeListener(onCheckListener);

        showSelection = (Button)findViewById(R.id.button3);
        showSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton checkedRadio = (RadioButton) findViewById(radMood.getCheckedRadioButtonId());
                String outputStr = checkedRadio.getText().toString();
                if(chkJava.isChecked() && chkKotlin.isChecked()){
                    outputStr+= " and I have Selected Android - Java and Android - Kotlin";
                }
                else if(chkJava.isChecked() && !chkKotlin.isChecked()){
                    outputStr+= " and I have Selected Android - Java";
                }
                else if(!chkJava.isChecked() && chkKotlin.isChecked()){
                    outputStr+= " and I have Selected Android - Kotlin";
                }
                else{
                    outputStr+= "";
                }

                Toast.makeText(getApplicationContext(), outputStr, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
